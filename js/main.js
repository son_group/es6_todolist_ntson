import {
  getNewTaskFromForm,
  renderTask,
} from "./controller/task.controller.js";

let arrTask = [];
let length = arrTask.length;
console.log(length);

function addTask() {
  let inputvalue = document.getElementById("newTask").value;
  if (inputvalue.length > 0) {
    let task = getNewTaskFromForm(arrTask);
    arrTask.push(task);
  }
  renderTask(arrTask);
}

function deleteTask(id) {
  console.log("delete task id ", id);
  let index = arrTask.findIndex((item) => item.id == id);
  console.log("index ", index);
  if (index != -1) {
    arrTask.splice(index, 1);
  }
  renderTask(arrTask);
}

function doneTask(id) {
  let index = arrTask.findIndex((item) => item.id == id);
  console.log("index ", index);
  if (index != -1) {
    if (arrTask[index].status == "new") {
      arrTask[index].status = "done";
    } else {
      arrTask[index].status = "new";
    }
  }
  renderTask(arrTask);
}

function sortTask(increase) {
  let sortArr = arrTask.sort((nextTask, currentTask) => {
    let nextTaskName = nextTask.name.toLowerCase();
    let currentTaskName = currentTask.name.toLowerCase();
    return nextTaskName.localeCompare(currentTaskName);
  });
  if (increase == true) {
    renderTask(sortArr);
  } else {
    renderTask(sortArr.reverse());
  }
}

function filterDone() {
  let filterArr = arrTask.filter((item) => item.status == "done");
  renderTask(filterArr);
}

function filterTime() {
  let sortTime = arrTask.sort((nextTask, currentTask) => {
    let nextTaskTime = nextTask.id;
    let currentTaskTime = currentTask.id;
    if (nextTaskTime > currentTaskTime) {
      return 1;
    }
    if (nextTaskTime < currentTaskTime) {
      return -1;
    }
    return 1;
  });

  renderTask(sortTime);
}

window.addTask = addTask;
window.deleteTask = deleteTask;
window.doneTask = doneTask;
window.sortTask = sortTask;
window.filterTime = filterTime;
window.filterDone = filterDone;
