export class Task {
  id;
  name;
  status;
  constructor(id, name, status) {
    this.id = id;
    this.name = name;
    this.status = status;
  }
}

// export default Task;
