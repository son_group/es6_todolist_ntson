import { Task } from "../model/task.model.js";

export let getNewTaskFromForm = (arrTask) => {
  let id;
  let length = arrTask.length;
  if (length == 0) {
    id = 1;
  } else {
    let index = arrTask.length - 1;
    id = arrTask[index].id + 1;
  }
  let name = document.getElementById("newTask").value;
  let status = "new";
  return new Task(id, name, status);
};

export let renderTask = (arrTask) => {
  let newContentHTML = "";
  let doneContentHTML = "";
  arrTask.forEach((task) => {
    let liContentHTML = `<li>${task.name}<span>
    <button class="btn btn-delete" onclick=deleteTask('${task.id}')><i class="fa fa-trash" aria-hidden="true"></i></button>
    <button class="btn"><i class="fa fa-check" onclick=doneTask('${task.id}')></i><button></span></li>`;
    if (task.status == "new") {
      newContentHTML += liContentHTML;
    } else {
      doneContentHTML += liContentHTML;
    }
  });
  document.getElementById("todo").innerHTML = newContentHTML;
  document.getElementById("completed").innerHTML = doneContentHTML;
};
